<?php  
 
return [

	/**
     * Set if the response will be JSON API formatted. Set TRUE as default
     */
	'json-api-format' => false,

	'content-type' => 'application/json',
 
	'api-version' => "1.0",

	'meta' => [

		'copyright' => "Todos los derechos reservados, YTicket 2015.",

		'authors' => ["JM Tech S.A.C."]
	
	],
 
];