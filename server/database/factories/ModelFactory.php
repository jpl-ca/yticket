<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(YTicket\Models\User::class, function ($faker) {

    return [
        'user_type_id' => 3,
        'department_id' => null,
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt('password'),
        'remember_token' => null,
    ];

});

$factory->defineAs(YTicket\Models\User::class, 'admin', function ($faker) use ($factory) {

    $user = $factory->raw(YTicket\Models\User::class);

    return array_merge($user, ['user_type_id' => 1]);
});

$factory->defineAs(YTicket\Models\User::class, 'agent', function ($faker) use ($factory) {

    $departments = YTicket\Models\Department::all()->lists('id')->toArray();
    $department_id = $faker->randomElement( $departments );

    $user = $factory->raw(YTicket\Models\User::class);

    return array_merge($user, [
        'user_type_id' => 2,
        'department_id' => $department_id,
    ]);

});

$factory->define(YTicket\Models\Topic::class, function ($faker) {

	$departments = YTicket\Models\Department::all()->lists('id')->toArray();

	$department = $faker->randomElement( $departments );

    return [
        'name' => $faker->word,
        'department_id' => $department
    ];

});

$factory->define(YTicket\Models\Engineer::class, function ($faker) {

    return [
        'name' => $faker->name,        
        'code' => $faker->unique()->randomNumber(5),
        'email' => $faker->optional()->freeEmail,
        'phone' => $faker->optional()->randomNumber(9)
    ];

});

$factory->define(YTicket\Models\Ticket::class, function ($faker) {

    $shift_id = $faker->randomElement( YTicket\Models\Shift::all()->lists('id')->toArray() );
    $engineer_id = $faker->randomElement( YTicket\Models\Engineer::all()->lists('id')->toArray() );
    $network_type_id = $faker->randomElement( YTicket\Models\NetworkType::all()->lists('id')->toArray() );
    $topic_id = $faker->randomElement( YTicket\Models\Topic::all()->lists('id')->toArray() );
    $affected_services = $faker->numberBetween($min = 0, $max = 1);
    $start_date = ($affected_services == 0) ? null : \Carbon\Carbon::now()->addHours($faker->numberBetween($min = 1, $max = 24))->toDateTimeString();
    $end_date = ($affected_services == 0) ? null : \Carbon\Carbon::now()->addHours($faker->numberBetween($min = 24, $max = 48))->toDateTimeString();
    $call_time = \Carbon\Carbon::now()->addHours($faker->numberBetween($min = 1, $max = 8))->toDateTimeString();

    return [
        'code' => $faker->unique()->randomNumber(7),
        'user_id' => null,
        'shift_id' => $shift_id,
        'engineer_id' => $engineer_id,
        'network_type_id' => $network_type_id,
        'affected_services' => $affected_services,
        'start_date' => $start_date,
        'end_date' => $end_date,
        'call_time' => $call_time,
        'topic_id' => $topic_id,
        'subject' => $faker->sentence($nbWords = 6),
        'detail' => $faker->text($maxNbChars = 1000),
        'ticket_state_id' => 1,
    ];

});

$factory->define(YTicket\Models\TicketReply::class, function ($faker) {

    $ticket_id = $faker->randomElement( YTicket\Models\Ticket::all()->lists('id')->toArray() );

    return [
        'ticket_id' => $ticket_id,
        'user_id' => null,
        'detail' => $faker->text($maxNbChars = 1200)
    ];

});

$factory->defineAs(YTicket\Models\TicketReply::class, 'agent_reply', function ($faker) use ($factory) {

    $ticket = $factory->raw(YTicket\Models\TicketReply::class);
    $hours = $faker->randomElement( array(1,3,5) );

    return array_merge($ticket, [
        'created_at' => \Carbon\Carbon::now()->addHours( $hours )->toDateTimeString(),
        'updated_at' => \Carbon\Carbon::now()->addHours( $hours  )->toDateTimeString()
    ]);

});

$factory->defineAs(YTicket\Models\TicketReply::class, 'user_reply', function ($faker) use ($factory) {

    $ticket = $factory->raw(YTicket\Models\TicketReply::class);
    $hours = $faker->randomElement( array(2,4) );

    return array_merge($ticket, [
        'created_at' => \Carbon\Carbon::now()->addHours( $hours )->toDateTimeString(),
        'updated_at' => \Carbon\Carbon::now()->addHours( $hours  )->toDateTimeString()
    ]);

});