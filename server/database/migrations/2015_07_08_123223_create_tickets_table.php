<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('shift_id')->unsigned();
            $table->integer('engineer_id')->unsigned();
            $table->integer('network_type_id')->unsigned();
            $table->boolean('affected_services')->default(0);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->dateTime('call_time');
            $table->integer('topic_id')->unsigned();
            $table->string('subject');
            $table->text('detail');
            $table->integer('ticket_state_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('shift_id')->references('id')->on('shifts');
            $table->foreign('engineer_id')->references('id')->on('engineers');
            $table->foreign('network_type_id')->references('id')->on('network_types');
            $table->foreign('topic_id')->references('id')->on('topics');
            $table->foreign('ticket_state_id')->references('id')->on('ticket_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
    }
}
