<?php

use Illuminate\Database\Seeder;

class EngineerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('YTicket\Models\Engineer', 20)->create();
    }
}
