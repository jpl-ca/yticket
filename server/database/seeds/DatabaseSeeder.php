<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTypeTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(TopicTableSeeder::class);
        $this->call(ShiftTableSeeder::class);
        $this->call(NetworkTypeTableSeeder::class);
        $this->call(TicketStateTableSeeder::class);
        $this->call(EngineerTableSeeder::class);
        $this->call(UserTableSeeder::class);

        Model::reguard();
    }
}
