<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'yticket@yopmail.com',
            'user_type_id' => 1,
            'password' => bcrypt('password'),
            'created_at' => Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon\Carbon::now()->toDateTimeString()
        ]);

        factory('YTicket\Models\User', 50)->create()->each(function($u) use($faker) {
            $tq = $faker->numberBetween($min = 2, $max = 25);
            $u->tickets()->saveMany(factory('YTicket\Models\Ticket', $tq)->make());
            $u->ticket_replies()->saveMany(factory('YTicket\Models\TicketReply', 'user_reply', 2)->make());
        });

        factory('YTicket\Models\User', 'agent', 10)->create()->each(function($u) use($faker) {
            $u->ticket_replies()->saveMany(factory('YTicket\Models\TicketReply', 'agent_reply', 3)->make());
        });
    }
}
