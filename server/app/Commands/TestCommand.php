<?php

namespace YTicket\Commands;

use YTicket\Commands\Command;
use Illuminate\Contracts\Bus\SelfHandling;

class TestCommand extends Command implements SelfHandling
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
