<?php

namespace YTicket\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

use PaulVL\JsonApi\Response as JsonApiResponse;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * The PaulVL\JsonAPi response.
     *
     * @var Guard
     */
    private $jsonapiresponse;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->jsonapiresponse = new JsonApiResponse;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()) {
            return $this->jsonapiresponse->responseForbidden();
            //return redirect('/home');
        }

        return $next($request);
    }
}
