<?php

namespace YTicket\Http\Middleware;

use Closure;

class AccessControlAllow {

    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        header("Access-Control-Allow-Origin: http://192.168.1.19:9000");
        header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With, *');
        header('Access-Control-Allow-Credentials: true');

        if (!$request->isMethod('options')) {
                return $next($request);
        }
    }

}