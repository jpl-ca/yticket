<?php

namespace YTicket\Http\Controllers\API;

use Illuminate\Http\Request;

use PaulVL\JsonApi\Controller as JsonApiController;
use PaulVL\JsonApi\Response;

class UserController extends JsonApiController
{
	protected $model_class = 'YTicket\Models\User';

    protected $json_api_format = false;
    
    public function saveData(array $inputs) {
        try {
            $user = new \YTicket\Models\User;
            $user->user_type_id = $inputs['user_type_id'];
            $user->name = $inputs['name'];
            $user->email = $inputs['email'];
            $user->password = bcrypt($inputs['password']);

            $user->save();
            $response = new Response;
            $response->handleData($user);
            return $response->responseCreated();
        } catch (Exception $e) {
            return $response->responseInternalServerError();
        }   
    }
    
    public function updateData($user, array $inputs) {
        try {
            $user->user_type_id = $inputs['user_type_id'];
            $user->name = $inputs['name'];
            $user->email = $inputs['email'];
            $user->password = bcrypt($inputs['password']);

            $user->save();
            $response = new Response;
            $response->handleData($user);
            return $response->response();
        } catch (Exception $e) {
            return $response->responseInternalServerError();
        }   
    }
    
}
