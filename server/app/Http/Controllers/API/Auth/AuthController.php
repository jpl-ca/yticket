<?php

namespace YTicket\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Validator;

use YTicket\Models\User;
use YTicket\Http\Controllers\Controller;

use PaulVL\JsonApi\Response as JsonApiResponse;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['only' => ['postLogin']]);
        $this->middleware('auth', ['only' => ['getLogout']]);
        /*Uncomment on production

        */
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }   

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $response = new JsonApiResponse;

        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        if (Auth::attempt($this->getCredentials($request), $request->has('remember'))) {
            
            //$response->withoutRelations();
            $response->handleData(Auth::user());
            
            return $response->response();
        }

        return $response->responseUnprocessableEntity( $this->getFailedLoginMessage() );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only($this->loginUsername(), 'password');
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return trans('auth.failed');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout(Request $request)
    {
        $response = new JsonApiResponse;
        
        if($request->user()) {            
            Auth::logout();            
        }
        return $response->response();
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCheck(Request $request)
    {
        $response = new JsonApiResponse;
        if($request->user()) {
            $response->handleData($request->user());            
            return $response->response();
        }
        return $response->responseUnauthorized();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }
}
