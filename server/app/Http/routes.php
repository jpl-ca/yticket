<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group( [ 'namespace' => 'API', 'prefix' => 'api' ], function ()
{

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);
	
	Route::resource('user', 'UserController', ['except' => ['create', 'edit']]);

	Route::resource('department', 'DepartmentController', ['except' => ['create', 'edit']]);

	Route::resource('engineer', 'EngineerController', ['except' => ['create', 'edit']]);

	Route::resource('network-type', 'NetworkTypeController', ['except' => ['create', 'edit']]);

	Route::resource('shift', 'ShiftController', ['except' => ['create', 'edit']]);

	Route::resource('ticket', 'TicketController', ['except' => ['create', 'edit']]);

	Route::resource('ticket-reply', 'TicketReplyController', ['except' => ['create', 'edit']]);

	Route::resource('ticket-state', 'TicketStateController', ['except' => ['create', 'edit']]);

	Route::resource('topic', 'TopicController', ['except' => ['create', 'edit']]);

	Route::resource('user-type', 'UserTypeController', ['except' => ['create', 'edit']]);
	
});

/**
 * TEST
 */
Route::get('/test', function (Illuminate\Http\Request $request) {


    $e = YTicket\Models\User::orderBy('created_at', 'ASC');

    $query = $request->get('q', null);

    $_conditions = explode("|", $request->get('q', null));

    $conditions = array();

    
    foreach ($_conditions as $key => $parameters) {
    	parse_str($parameters, $conditions[$key]);
    }

    $queries = array();

	foreach($conditions as $item) {
	    $condition = key($item);
	    $values = current($item);

	    if(!isset($result[$condition])) {
	        $result[$condition] = array();
	    }
	    $queries[$condition][] = explode(",", $values);
	}

    return dd($queries);
});
/**
 * TEST
 */
Route::post('/test', function (Illuminate\Http\Request $request) {
	
    $response = new PaulVL\JsonApi\Response();

    $inputs = $request->all();

    Log::debug($inputs);

    return $response->response();

    $query = $request->get('q', null);
    if(!empty($query)) {
        return dd(json_decode($query, true));
    }
    return 'sin query';
    exit();
	$class = $this->model_class;
    /* MANUAL DATA HANDLING
	$handler = new DataHandler($class::all());
    $response->data = $handler->getApiJsonableData(true);
    */
    $response->withoutRelations();
    $response->handleData($class::all());
    return $response->response();
});