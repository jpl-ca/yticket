<?php

namespace YTicket\Models;

use PaulVL\JsonApi\Model;

class TicketReply extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from queries and the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The relationship included on the model's JSON form.
     *
     * @var array
     */
    protected $displayable_relations = [
    	//'relation_example'
    	];

    /**
     * Validation rules. If any field needs unique validation, add this rule at the end of the line like example
     *
     * @var array
     */
    static protected $rules = [
        //'field_example' => 'required|email|max:255|unique:table,column'
    ];

    /*
    public function relation_example() {
        return $this->belongsTo('Class', 'foreign_key', 'primary_key');
    }
    */

    public function user() {
        return $this->belongsTo('YTicket\Models\User', 'user_id', 'id');
    }

    public function ticket() {
        return $this->belongsTo('YTicket\Models\ticket', 'ticket_id', 'id');
    }
}