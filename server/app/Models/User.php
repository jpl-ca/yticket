<?php

namespace YTicket\Models;

use Illuminate\Auth\Authenticatable;
use PaulVL\JsonApi\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'user_type_id', 'department_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $displayable_relations = ['user_type', 'department'];

    static protected $rules = [
        'user_type_id' => 'required|integer|exists:user_types,id',
        'department_id' => 'required_if:user_type_id,2|integer|exists:departments,id',
        'name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users,email',
        'password' => 'required|confirmed|min:6',
    ];

    public function user_type() {
        return $this->belongsTo('YTicket\Models\UserType', 'user_type_id', 'id');
    }

    public function department() {
        return $this->belongsTo('YTicket\Models\Department', 'department_id', 'id');
    }

    public function tickets() {
        return $this->hasMany('YTicket\Models\Ticket', 'user_id', 'id');
    }

    public function ticket_replies() {
        return $this->hasMany('YTicket\Models\TicketReply', 'user_id', 'id');
    }
}
