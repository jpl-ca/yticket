<?php

namespace YTicket\Models;

use PaulVL\JsonApi\Model;

class Ticket extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from queries and the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The relationship included on the model's JSON form.
     *
     * @var array
     */
    protected $displayable_relations = [
        'user',
        'topic',
        'ticket_state',
        'network_type',
        'engineer',
        'shift',
    	'ticket_replies'
    	];

    /**
     * Validation rules. If any field needs unique validation, add this rule at the end of the line like example
     *
     * @var array
     */
    static protected $rules = [
        //'field_example' => 'required|email|max:255|unique:table,column'
    ];

    /*
    public function relation_example() {
        return $this->belongsTo('Class', 'foreign_key', 'primary_key');
    }
    */

    public function user() {
        return $this->belongsTo('YTicket\Models\User', 'user_id', 'id');
    }

    public function topic() {
        return $this->belongsTo('YTicket\Models\Topic', 'topic_id', 'id');
    }

    public function ticket_state() {
        return $this->belongsTo('YTicket\Models\TicketState', 'ticket_state_id', 'id');
    }

    public function network_type() {
        return $this->belongsTo('YTicket\Models\NetworkType', 'network_type_id', 'id');
    }

    public function engineer() {
        return $this->belongsTo('YTicket\Models\Engineer', 'engineer_id', 'id');
    }

    public function shift() {
        return $this->belongsTo('YTicket\Models\Shift', 'shift_id', 'id');
    }

    public function ticket_replies() {
        return $this->hasMany('YTicket\Models\TicketReply', 'ticket_id', 'id');
    }
}